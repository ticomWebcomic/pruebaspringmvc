/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.hash.springmvc.controles;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MensajeController {
    @GetMapping("/mensaje")
    public String mostrarMensaje() {        
        System.out.println("Mostrando data");
        return "mensaje";
    }
}
