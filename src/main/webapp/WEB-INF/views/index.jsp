<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>CSS Grid Test</title>

    <!-- import the webpage's stylesheet -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- import the webpage's javascript file -->
    <script src="js/script.js" type="module"></script>
  </head>
  <body>
    <div id="kinheader">
      <img
        id="kinlogo"
        src="img/ticom_web.png"
        alt="Logo TICOM"
      />

      <h1 id="kinwelcome">Prueba de contenido estatico en Spring MVC</h1>
      <div class="glitchButton"></div>
    </div>

    <div id="kinavatar">
      <img
        src="img/pixel_hash.png"
        alt="Avatar"
        height="150px"
        width="150px"
      />
    </div>

    <div id="kincontenido">
      <div>
        Esta es una prueba de contenido estatico en Spring MVC
      </div>
      <div>
          Este enlace lo lleva a otra parte de la aplicacion: <br /><a href="mensaje">Ir a Mensaje</a>
      </div>
      <div>
        Quibusdam, eos esse dolorum facere voluptatem eius, dolore quas totam
        aspernatur obcaecati harum? Nihil eligendi eos minus odit minima earum
        incidunt rem fugit reprehenderit, molestiae possimus eveniet itaque
        laudantium excepturi.
      </div>
      <div>
        Ducimus quibusdam inventore delectus doloribus dignissimos. Dignissimos
        quos officia minus exercitationem perspiciatis harum iusto molestiae
        deleniti quod sunt amet recusandae autem, neque doloremque ad alias
        eaque consequuntur nesciunt quis eius!
      </div>
      <div>
        Cumque aspernatur ex ipsum dolorum eius, tempore omnis minus sequi
        architecto totam sunt maxime nemo, ab repellendus. Aut voluptatem saepe
        voluptatibus nisi ipsum. Debitis corporis culpa ipsa error nemo
        doloribus.
      </div>
      <div>
        Consequatur dolore, architecto quos saepe consequuntur libero minus
        totam? Enim optio provident commodi corporis officiis, sunt maiores?
        Cupiditate consequuntur, cumque natus corporis velit sunt ad magni
        aliquid facere deleniti molestiae.
      </div>
      <div>
        Voluptatibus similique modi voluptatum voluptatem quo quod minima
        ducimus facere, sequi libero accusamus nisi nobis? Minima error tempore
        quo esse quod odit, deleniti labore nulla ullam velit nemo neque sint!
      </div>
      <div>
        Qui, corporis delectus? Pariatur vel autem commodi, accusantium,
        voluptate obcaecati iste, a debitis facilis repellendus mollitia. Dolore
        dicta totam, quaerat omnis accusantium magni alias voluptates eligendi
        ex id aut dolorem?
      </div>
      <div>
        Recusandae tempora ab error omnis exercitationem illo accusamus esse sit
        ipsa accusantium iure, possimus ducimus quis consequuntur qui corporis
        nobis culpa repudiandae! Suscipit, debitis. Omnis delectus at vitae
        laborum quos?
      </div>
      <div>
        Dolorem saepe accusamus sed placeat porro ex, ab, vel eaque libero
        incidunt facilis delectus, iure odio dicta error consequuntur
        perspiciatis quasi? Corrupti incidunt quia asperiores quo magnam at
        minima laudantium?
      </div>
      <div>
        Dolor ad saepe, nemo fugit tempora autem est fugiat quis porro atque nam
        repellendus maxime neque voluptatem rerum amet odit aspernatur
        voluptates iusto eos laboriosam enim vel. Eius, debitis beatae!
      </div>
    </div>
    <p>
      <script src="https://ko-fi.com/widgets/widget_2.js"></script>
      <script>
        kofiwidget2.init("Support Me on Ko-fi", "#28c1e0", "Q5Q0EMQV");
        kofiwidget2.draw();
      </script>

      <br />

      I'm your cool new webpage. Made with
      <a href="https://glitch.com">Glitch</a>!
    </p>

    <!-- include the Glitch button to show what the webpage is about and
          to make it easier for folks to view source and remix -->
    <div class="glitchButton" style="position:fixed;top:20px;right:20px;"></div>
    <script src="https://button.glitch.me/button.js" defer></script>
  </body>
</html>
