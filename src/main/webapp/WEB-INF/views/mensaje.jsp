<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>CSS Grid Test</title>

    <!-- import the webpage's stylesheet -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- import the webpage's javascript file -->
    <script src="js/script.js" type="module"></script>
  </head>
  <body>
    <div id="kinheader">
      <img
        id="kinlogo"
        src="img/ticom_web.png"
        alt="Logo TICOM"
      />

      <h1 id="kinwelcome">Mensaje</h1>
      <div class="glitchButton"></div>
    </div>

    <div id="kinavatar">
      <img
        src="img/pixel_hash.png"
        alt="Avatar"
        height="150px"
        width="150px"
      />
    </div>

    <div id="kincontenido">
      <div>
          <a href="/PruebaSpringMVC/">Regresar</a>
      </div>
      <div>
          HOLA MUNDO!
      </div>
    </div>
    <p>
      <script src="https://ko-fi.com/widgets/widget_2.js"></script>
      <script>
        kofiwidget2.init("Support Me on Ko-fi", "#28c1e0", "Q5Q0EMQV");
        kofiwidget2.draw();
      </script>

      <br />

      I'm your cool new webpage. Made with
      <a href="https://glitch.com">Glitch</a>!
    </p>

    <!-- include the Glitch button to show what the webpage is about and
          to make it easier for folks to view source and remix -->
    <div class="glitchButton" style="position:fixed;top:20px;right:20px;"></div>
    <script src="https://button.glitch.me/button.js" defer></script>
  </body>
</html>
